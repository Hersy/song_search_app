package com.hersy.songsearchapp.data.models

import com.google.gson.annotations.SerializedName

data class SearchResponse(
    @SerializedName("results")
    val results: Results
)