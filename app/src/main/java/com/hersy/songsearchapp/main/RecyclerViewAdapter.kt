package com.hersy.songsearchapp.main


import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.hersy.songsearchapp.R
import com.hersy.songsearchapp.data.models.Track
import com.squareup.picasso.Picasso

class RecyclerViewAdapter(private var context: Activity, songList: ArrayList<Track>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var songArrayList: ArrayList<Track> = songList
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val rootView: View = LayoutInflater.from(context).inflate(R.layout.song_item, parent, false)
        return RecyclerViewViewHolder(rootView)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val track: Track = songArrayList[position]
        val viewHolder = holder as RecyclerViewViewHolder
        if (track.image.any { it.size == "small" } && (track.image.last { it.size == "small"}).text!=""){
            Picasso.get().load((track.image.last { it.size == "small"}).text).into(viewHolder.ivAlbum)
        }
        viewHolder.tvSongTitle.text = track.name
        viewHolder.tvArtist.text = track.artist
        viewHolder.tvListeners.text = track.listeners
    }

    override fun getItemCount(): Int {
        return songArrayList.size
    }

    internal class RecyclerViewViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var ivAlbum: ImageView = itemView.findViewById(R.id.ivAlbum)
        var tvSongTitle: TextView = itemView.findViewById(R.id.tvSongTitle)
        var tvArtist: TextView = itemView.findViewById(R.id.tvArtist)
        var tvListeners: TextView = itemView.findViewById(R.id.tvListeners)
    }

}