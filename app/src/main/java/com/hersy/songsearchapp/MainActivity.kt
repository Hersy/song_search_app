package com.hersy.songsearchapp

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.core.view.isVisible
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.hersy.songsearchapp.databinding.ActivityMainBinding
import com.hersy.songsearchapp.main.MainViewModel
import com.hersy.songsearchapp.main.RecyclerViewAdapter
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    private val viewModel: MainViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.switchPopular.setOnCheckedChangeListener { _, b ->
            if (b){
                viewModel.setSortingMethod(true)
                binding.tvPopularState.text = getString(R.string.lb_most_popular)
            }else{
                viewModel.setSortingMethod(false)
                binding.tvPopularState.text = getString(R.string.lb_least_popular)
            }
        }

        binding.svSongTitle.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                viewModel.searchSongs(query)
                return false
            }
            override fun onQueryTextChange(newText: String?): Boolean {
                return false
            }
        })

        binding.rvSongs.layoutManager = LinearLayoutManager(this)
        viewModel.songsLiveData.observe(this, {
            binding.rvSongs.adapter = RecyclerViewAdapter(this, it)
        })

        lifecycleScope.launchWhenStarted {
            viewModel.search.collect { event ->
                when(event) {
                    is MainViewModel.SongSearchEvent.Success -> {
                        binding.progressBar.isVisible = false
                        binding.tvError.isVisible = false
                        binding.rvSongs.isVisible = true
                    }
                    is MainViewModel.SongSearchEvent.Failure -> {
                        binding.progressBar.isVisible = false
                        binding.tvError.text = event.errorText
                        binding.tvError.isVisible = true
                        binding.rvSongs.isVisible = false
                    }
                    is MainViewModel.SongSearchEvent.Loading -> {
                        binding.rvSongs.isVisible = false
                        binding.tvError.isVisible = false
                        binding.progressBar.isVisible = true
                    }
                    else -> Unit
                }
            }
        }
    }
}