package com.hersy.songsearchapp.utils

import com.hersy.songsearchapp.data.MockTrackList
import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class SongListUtilTest {

    @Test
    fun `sorting a mock song list from most to least popular`(){

        val result = SongListUtil.sortList(
            true,
            MockTrackList.mockList
        )
        assertEquals(result, MockTrackList.descendantOrderMockList)
    }

    @Test
    fun `sorting a the same mock song list from least to most popular`(){

        val result = SongListUtil.sortList(
            false,
            MockTrackList.mockList
        )
        assertEquals(result, MockTrackList.ascendantOrderdMockList)
    }

    @Test
    fun `sorting a similar mock list, but one of the "listener" strings is not a number, from least to most popular`(){

        val result = SongListUtil.sortList(
            false,
            MockTrackList.malformedMockList
        )
        assertEquals(result, MockTrackList.ascendantMalformedOrderdMockList)
    }

}