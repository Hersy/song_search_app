package com.hersy.songsearchapp.utils

import com.hersy.songsearchapp.data.models.Track

object SongListUtil {

    fun sortList(mostPopular: Boolean, list:ArrayList<Track>): ArrayList<Track>{
        list.removeAll{ it.listeners.toIntOrNull() == null }
        return if (mostPopular){
            ArrayList(list.sortedByDescending { it.listeners.toIntOrNull() })
        }else{
            ArrayList(list.sortedBy { it.listeners.toIntOrNull() })
        }
    }

}