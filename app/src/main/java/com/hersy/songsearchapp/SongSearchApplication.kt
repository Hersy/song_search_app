package com.hersy.songsearchapp

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class SongSearchApplication : Application()