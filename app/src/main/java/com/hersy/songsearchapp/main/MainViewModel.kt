package com.hersy.songsearchapp.main

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.hersy.songsearchapp.data.models.Track
import com.hersy.songsearchapp.utils.DispatcherProvider
import com.hersy.songsearchapp.utils.Resource
import com.hersy.songsearchapp.utils.SongListUtil.sortList
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

class MainViewModel @ViewModelInject constructor(
        private val repository: MainRepository,
        private val dispatchers: DispatcherProvider
): ViewModel() {

    var songsLiveData = MutableLiveData<ArrayList<Track>>()

    sealed class SongSearchEvent {
        object Success: SongSearchEvent()
        class Failure(val errorText: String): SongSearchEvent()
        object Loading : SongSearchEvent()
        object Empty : SongSearchEvent()
    }

    sealed class SortEvent {
        object MostPopular : SortEvent()
        object LeastPopular : SortEvent()
    }

    private val _search = MutableStateFlow<SongSearchEvent>(SongSearchEvent.Empty)
    private val _sort = MutableStateFlow<SortEvent>(SortEvent.MostPopular)

    val search: StateFlow<SongSearchEvent> = _search

    fun searchSongs(track: String?) {
        if(track == null || track == "") {
            songsLiveData.value?.clear()
            _search.value = SongSearchEvent.Empty
            return
        }

        viewModelScope.launch(dispatchers.io) {
            _search.value = SongSearchEvent.Loading
            when(val searchResponse = repository.getSongs(track)) {
                is Resource.Error -> {
                    _search.value = SongSearchEvent.Failure(searchResponse.message!!)
                }
                is Resource.Success -> {
                    songsLiveData.value?.clear()
                    songsLiveData.postValue(sortList(_sort.value == SortEvent.MostPopular, ArrayList(searchResponse.data!!.results.trackmatches.track )))
                    _search.value = SongSearchEvent.Success
                }
            }
        }
    }

    fun setSortingMethod(mostPopular: Boolean){
        if (mostPopular){
            _sort.value=SortEvent.MostPopular
        }else{
            _sort.value=SortEvent.LeastPopular
        }

        if (songsLiveData.value!=null && songsLiveData.value!!.isNotEmpty()){
            val currentSongs = songsLiveData.value!!
            songsLiveData.postValue(sortList(mostPopular, currentSongs))
        }
    }

}