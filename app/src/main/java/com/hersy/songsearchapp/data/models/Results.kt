package com.hersy.songsearchapp.data.models

import com.google.gson.annotations.SerializedName

data class Results(
    @SerializedName("@attr")
    val attr: Attr,
    @SerializedName("opensearch:Query")
    val query: OpensearchQuery,
    @SerializedName("opensearch:itemsPerPage")
    val itemsPerPage: String,
    @SerializedName("opensearch:startIndex")
    val startIndex: String,
    @SerializedName("#opensearch:totalResults")
    val totalResults: String,
    @SerializedName("trackmatches")
    val trackmatches: Trackmatches
)