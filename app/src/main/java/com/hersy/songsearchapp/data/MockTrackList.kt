package com.hersy.songsearchapp.data

import com.hersy.songsearchapp.data.models.Track

object MockTrackList{

    /** Fake track list */
    val mockList = arrayListOf(
        Track("Tool", listOf(), "6455", "", "Lateralus", "", "" ),
        Track("Tool", listOf(), "41354", "", "Schism", "", "" ),
        Track("Tool", listOf(), "93467", "", "Ticks & Leeches", "", "" ),
        Track("Tool", listOf(), "4324", "", "Forty Six & 2", "", "" ),
        Track("Tool", listOf(), "5115", "", "Mantra", "", "" ),
        Track("Tool", listOf(), "7456", "", "The Pot", "", "" ),
        Track("Tool", listOf(), "12315", "", "Disposition", "", "" )
    )

    val descendantOrderMockList = arrayListOf(
        Track("Tool", listOf(), "93467", "", "Ticks & Leeches", "", "" ),
        Track("Tool", listOf(), "41354", "", "Schism", "", "" ),
        Track("Tool", listOf(), "12315", "", "Disposition", "", "" ),
        Track("Tool", listOf(), "7456", "", "The Pot", "", "" ),
        Track("Tool", listOf(), "6455", "", "Lateralus", "", "" ),
        Track("Tool", listOf(), "5115", "", "Mantra", "", "" ),
        Track("Tool", listOf(), "4324", "", "Forty Six & 2", "", "" )
    )

    val ascendantOrderdMockList = arrayListOf(
        Track("Tool", listOf(), "4324", "", "Forty Six & 2", "", "" ),
        Track("Tool", listOf(), "5115", "", "Mantra", "", "" ),
        Track("Tool", listOf(), "6455", "", "Lateralus", "", "" ),
        Track("Tool", listOf(), "7456", "", "The Pot", "", "" ),
        Track("Tool", listOf(), "12315", "", "Disposition", "", "" ),
        Track("Tool", listOf(), "41354", "", "Schism", "", "" ),
        Track("Tool", listOf(), "93467", "", "Ticks & Leeches", "", "" )
    )


    /** Fake track list with a non numeric "listeners" value */
    val malformedMockList = arrayListOf(
        Track("Tool", listOf(), "6455", "", "Lateralus", "", "" ),
        Track("Tool", listOf(), "41354", "", "Schism", "", "" ),
        Track("Tool", listOf(), "93467", "", "Ticks & Leeches", "", "" ),
        Track("Tool", listOf(), "not a number", "", "Forty Six & 2", "", "" ),
        Track("Tool", listOf(), "5115", "", "Mantra", "", "" ),
        Track("Tool", listOf(), "7456", "", "The Pot", "", "" ),
        Track("Tool", listOf(), "12315", "", "Disposition", "", "" )
    )

    /** Element that has a non numeric "listener" value has been removed (Forty Six & 2) */
    val ascendantMalformedOrderdMockList = arrayListOf(
        Track("Tool", listOf(), "5115", "", "Mantra", "", "" ),
        Track("Tool", listOf(), "6455", "", "Lateralus", "", "" ),
        Track("Tool", listOf(), "7456", "", "The Pot", "", "" ),
        Track("Tool", listOf(), "12315", "", "Disposition", "", "" ),
        Track("Tool", listOf(), "41354", "", "Schism", "", "" ),
        Track("Tool", listOf(), "93467", "", "Ticks & Leeches", "", "" )
    )

}