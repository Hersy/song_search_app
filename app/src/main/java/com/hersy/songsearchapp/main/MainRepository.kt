package com.hersy.songsearchapp.main

import com.hersy.songsearchapp.data.models.SearchResponse
import com.hersy.songsearchapp.utils.Resource

interface MainRepository {
    suspend fun getSongs(track: String): Resource<SearchResponse>
}