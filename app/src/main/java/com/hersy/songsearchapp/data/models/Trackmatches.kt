package com.hersy.songsearchapp.data.models

import com.google.gson.annotations.SerializedName

data class Trackmatches(
    @SerializedName("track")
    val track: List<Track>
)