package com.hersy.songsearchapp.main

import com.hersy.songsearchapp.BuildConfig
import com.hersy.songsearchapp.data.LastFmApi
import com.hersy.songsearchapp.data.models.SearchResponse
import com.hersy.songsearchapp.utils.Resource
import javax.inject.Inject

class DefaultMainRepository @Inject constructor(
    private val api: LastFmApi
) : MainRepository {

    override suspend fun getSongs(track: String): Resource<SearchResponse> {
        return try {
            val response = api.getSongs("track.search", track, BuildConfig.LAST_FM_API_KEY, "json")
            val result = response.body()
            if(response.isSuccessful && result != null) {
                Resource.Success(result)
            } else {
                Resource.Error(response.message())
            }
        } catch(e: Exception) {
            Resource.Error(e.message ?: "Null error")
        }
    }
}