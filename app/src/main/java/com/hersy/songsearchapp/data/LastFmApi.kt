package com.hersy.songsearchapp.data

import com.hersy.songsearchapp.data.models.SearchResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface LastFmApi {

    @GET("/2.0/")
    suspend fun getSongs(
        @Query("method") method: String,
        @Query("track") track: String,
        @Query("api_key") api_key: String,
        @Query("format") format: String
    ): Response<SearchResponse>
}